<?php
/**
 * @file
 * debut_search.context.inc
 */

/**
 * Implementation of hook_context_default_contexts().
 */
function debut_search_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'search-content';
  $context->description = '';
  $context->tag = 'Search';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'search' => 'search',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'facetapi-search_api@nodes:block:type' => array(
          'module' => 'facetapi',
          'delta' => 'search_api@nodes:block:type',
          'region' => 'sidebar_second',
          'weight' => '-17',
        ),
        'facetapi-search_api@nodes:block:created' => array(
          'module' => 'facetapi',
          'delta' => 'search_api@nodes:block:created',
          'region' => 'sidebar_second',
          'weight' => '-18',
        ),
        'facetapi-bEI7sfj-GE5kGkDqVUF_lL-c1U7dQXVq' => array(
          'module' => 'facetapi',
          'delta' => 'bEI7sfj-GE5kGkDqVUF_lL-c1U7dQXVq',
          'region' => 'sidebar_second',
          'weight' => '-19',
        ),
        'facetapi-search_api@nodes:current_search' => array(
          'module' => 'facetapi',
          'delta' => 'search_api@nodes:current_search',
          'region' => 'sidebar_second',
          'weight' => '-20',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Search');
  $export['search-content'] = $context;

  return $export;
}

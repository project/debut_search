<?php
/**
 * @file
 * debut_search.features.user_permission.inc
 */

/**
 * Implementation of hook_user_default_permissions().
 */
function debut_search_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer search_api
  $permissions['administer search_api'] = array(
    'name' => 'administer search_api',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'search_api',
  );

  return $permissions;
}

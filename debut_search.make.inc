; Drupal version
core = 7.x
api = 2

; Contrib modules
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.x-dev
projects[debut][subdir] = contrib
projects[debut][version] = 1.0-beta3
projects[debut_search][subdir] = contrib
projects[debut_search][version] = 1.x-dev
projects[features][subdir] = contrib
projects[features][version] = 1.0-beta4
projects[search_api][subdir] = contrib
projects[search_api][version] = 1.0-beta10
projects[search_api_db][subdir] = contrib
projects[search_api_db][version] = 1.0-beta1
projects[search_api_page][subdir] = contrib
projects[search_api_page][version] = 1.0-beta1
projects[search_api_solr][subdir] = contrib
projects[search_api_solr][version] = 1.0-beta4

libraries[SolrPhpClient][download][type] = "get"
libraries[SolrPhpClient][download][url] = "http://solr-php-client.googlecode.com/files/SolrPhpClient.r60.2011-05-04.tgz"
libraries[SolrPhpClient][directory_name] = "SolrPhpClient"
